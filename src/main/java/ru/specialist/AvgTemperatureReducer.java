package ru.specialist;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class AvgTemperatureReducer
  extends Reducer<Text, IntWritable, Text, IntWritable> {
  
  @Override
  public void reduce(Text key, Iterable<IntWritable> values,
      Context context)
      throws IOException, InterruptedException {
    
    int avgValue = 0;
    int count = 0;
    for (IntWritable value : values) {
      avgValue = avgValue+ value.get();
      count++;
    }
    avgValue=avgValue/count;
    context.write(key, new IntWritable(avgValue));
  }
}

